package br.com.desafio.dataprovider;

import br.com.desafio.testcase.BaseTest;
import org.testng.annotations.DataProvider;


public class DataProviders extends BaseTest {


    @DataProvider(name = "dataProviderSexoInvalidos")
    public static Object[][] dataProviderAuth() {
        return new Object[][]{
                {"M", 400},
                {"F", 400},
                {"I", 400},
                {"R", 400},
                {"O", 400},
        };
    }


}