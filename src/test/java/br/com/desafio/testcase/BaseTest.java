package br.com.desafio.testcase;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import br.com.desafio.util.RequestSpecificationBuilder;

@Listeners({ExtentITestListenerClassAdapter.class})
public class BaseTest {


    protected RequestSpecificationBuilder requestSpecBuilder = new RequestSpecificationBuilder();

    protected static RequestSpecification requestSpecification;

    @BeforeClass(alwaysRun = true)
    protected void before() {
        requestSpecification = requestSpecBuilder.build();
    }

}
