package br.com.desafio.testcase;

import br.com.desafio.dataprovider.DataProviders;
import com.github.javafaker.Faker;
import br.com.desafio.dto.EmpregadoDto;
import br.com.desafio.enums.JsonSchemas;
import br.com.desafio.factory.EmpregadoFactory;
import org.apache.commons.lang3.RandomUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;


public class FuncionalTest extends BaseTest {


    private List<EmpregadoDto> listEmp;

    @BeforeTest
    public void beforeTest() {

    }

    @Test(groups = "contrato")
    public void deveriaRetornarContratoEsperado() {

        given().
                spec(requestSpecification).
                when().
                get("empregado/list_all").
                then().
                statusCode(200).assertThat().body(matchesJsonSchema(new File(JsonSchemas.JSON_SCHEMA_GET_ALL.value())));

    }

    @Test(groups = "get")
    public void deveriaBuscarTodosEmpregados() {

        listEmp =
                given().
                        spec(requestSpecification).
                        when().
                        get("empregado/list_all").
                        then().
                        statusCode(200).
                        extract().body().jsonPath().getList(".", EmpregadoDto.class);

        Assert.assertNotNull(listEmp, ">>!FALHA AO BUSCAR LISTA DE EMPREGADOS!<<");
        listEmp = new ArrayList<>(listEmp);
    }

    @Test(groups = "get", dependsOnMethods = "deveriaBuscarTodosEmpregados")
    public void deveriaBuscarEmpregadoPorId() {

        EmpregadoDto empEsperado = listEmp.stream().skip(RandomUtils.nextInt(0, listEmp.size() - 1)).findAny().get();

        EmpregadoDto empRecebido =
                given().
                        spec(requestSpecification).
                        when().
                        pathParam("empregadoId", empEsperado.getEmpregadoId()).
                        get("empregado/list/{empregadoId}").
                        then().
                        statusCode(202).extract().as(EmpregadoDto.class);

        Assert.assertEquals(empRecebido, empEsperado, ">>!FALLHA AO BUSCAR EMPREGADO POR ID!<<");

    }

    @Test(groups = "post")
    public void deveriaCadastrarEmpregado() {

        EmpregadoDto empregado = new EmpregadoFactory().buildEmpregado();

        EmpregadoDto empEsperado =
                given().
                        spec(requestSpecification).
                        body(empregado).
                        when().
                        post("empregado/cadastrar").
                        then().
                        statusCode(202).extract().as(EmpregadoDto.class);

        EmpregadoDto empRecebido =
                given().
                        spec(requestSpecification).
                        when().
                        pathParam("empregadoId", empEsperado.getEmpregadoId()).
                        get("empregado/list/{empregadoId}").
                        then().
                        statusCode(202).extract().as(EmpregadoDto.class);

        Assert.assertEquals(empRecebido, empEsperado, ">>!FALLHA AO BUSCAR EMPREGADO POR ID!<<");

    }

    @Test(groups = "post", dataProviderClass = DataProviders.class, dataProvider = "dataProviderSexoInvalidos" )
    public void naoDeveriaCadastrarEmpregado(String sexo, Integer statusCode) {

        EmpregadoDto empregado = new EmpregadoFactory().buildEmpregado();
        empregado.setSexo(sexo);

                given().
                        spec(requestSpecification).
                        body(empregado).
                        when().
                        post("empregado/cadastrar").
                        then().
                        statusCode(statusCode);
    }

    @Test(groups = "put", dependsOnMethods = "deveriaBuscarTodosEmpregados")
    public void deveriaEditarEmpregado() {

        EmpregadoDto empEsperado = listEmp.stream().skip(RandomUtils.nextInt(0, listEmp.size() - 1)).findAny().get();
        empEsperado.setNome(Faker.instance().name().firstName());

        EmpregadoDto empRecebido =
                given().
                        spec(requestSpecification).
                        when().
                        pathParam("empregadoId", empEsperado.getEmpregadoId()).
                        put("empregado/alterar/{empregadoId}").
                        then().
                        statusCode(201).extract().as(EmpregadoDto.class);

        Assert.assertEquals(empRecebido, empEsperado, ">>!FALLHA AO EDITAR EMPREGADO!<<");
    }
}