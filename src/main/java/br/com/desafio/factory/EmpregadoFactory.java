package br.com.desafio.factory;

import com.github.javafaker.Faker;
import br.com.desafio.dto.EmpregadoDto;
import br.com.desafio.util.Utils;

public class EmpregadoFactory {

    public EmpregadoDto buildEmpregado() {

        return EmpregadoDto.builder()
                .admissao(Utils.getActualDateTime("dd/MM/YYYY", 0))
                .cargo("ANNALISTA DE QUALIDADE")
                .comissao("2.000,00")
                .departamentoId("1")
                .nome(Faker.instance().name().firstName())
                .sexo("i")
                .cpf(Utils.cpfGenarator(true, true))
                .salario("6.500,00")
                .tipoContratacao("clt")
                .build();

    }

}
