package br.com.desafio.enums;

public enum JsonSchemas {

    JSON_SCHEMA_GET_ALL("src/main/resources/jsonschema/listAllEmpregados.json");
	
    String valor;

    JsonSchemas(String valor) {
        this.valor = valor;
    }

    public String value() {
        return valor;
    }
}
