package br.com.desafio.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum SexoEnum {
    MASCULINO('M'),
    FEMININO('F');

    private final Character value;

    public static SexoEnum parse(Character value) {
        return Arrays.stream(SexoEnum.values()).filter(s -> s.value.equals(value)).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
