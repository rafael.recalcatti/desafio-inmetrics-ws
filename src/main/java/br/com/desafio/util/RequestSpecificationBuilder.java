package br.com.desafio.util;


import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;

public class RequestSpecificationBuilder {

    String baseUriService = "https://inm-api-test.herokuapp.com";

    String basePathService = "/";

    String user = "inmetrics";

    String pass = "automacao";


    public RequestSpecification build() {

        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

        PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
        authScheme.setUserName(user);
        authScheme.setPassword(pass);

        requestSpecBuilder.setConfig(new RestAssuredConfig()
                .sslConfig(new SSLConfig().relaxedHTTPSValidation()));
        requestSpecBuilder.setRelaxedHTTPSValidation();
        requestSpecBuilder.log(LogDetail.ALL);
        requestSpecBuilder.addFilter(new ResponseLoggingFilter());
        requestSpecBuilder.addHeader("Content-Type", "application/json");
        requestSpecBuilder.setAuth(authScheme);
        requestSpecBuilder.setBaseUri(baseUriService);
        requestSpecBuilder.setBasePath(basePathService);

        return requestSpecBuilder.build();
    }

}
